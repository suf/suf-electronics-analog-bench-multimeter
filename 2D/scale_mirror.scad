// upper_scale_distance = 64.3467;
upper_scale_distance = 61.75;
upper_scale = 100;
lower_scale_distance = 58.5;
lower_scale = 60;
scale_degree = 86;

color("black")
{
    for(i=[0:upper_scale])
    {
        length = i%10 == 0 ? 6 : (i%5 == 0 ? 4.5 : 3);
        rotate((scale_degree/2)-i*scale_degree/upper_scale)
        {
            if(i%10 == 0)
            {
                translate([0,upper_scale_distance + 7])
                {
                    text(str(i),size=2.5,font="Abadi Extra Light",halign="center");
                }
            }
            translate([0,upper_scale_distance + length/2])
            {
                square([0.1,length],center = true);
            }
        }
    }

    for(i=[0:lower_scale])
    {
        length = i%20 == 0 ? 6 : (i%10 == 0 ? 4.5 : (i%2 == 0 ? 3.5 : 2.5));
        rotate((scale_degree/2)-i*scale_degree/lower_scale)
        {
            if(i%10 == 0)
            {
                translate([0,lower_scale_distance-9])
                {
                    text(str(i/2),size=2.5,font="Abadi Extra Light",halign="center");
                }
            }
            translate([0,lower_scale_distance - length/2])
            {
                square([0.1,length],center = true);
            }
        }
    }
    // Allign cross
    square([0.1,10],center = true);
    square([10,0.1],center = true);
}