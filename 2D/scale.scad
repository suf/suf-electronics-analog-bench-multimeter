color("black")
{
    /*
    translate([0,27])
    {
        text("V",size=12,font="Abadi",halign="center");
    }
    */
    for(i=[0:100])
    {
        length = i%10 == 0 ? 6 : (i%5 == 0 ? 4.5 : 3);
        rotate(45-i*0.9)
        {
            if(i%10 == 0)
            {
                translate([0,67])
                {
                    text(str(i),size=2.5,font="Abadi Extra Light",halign="center");
                }
            }
            translate([0,60 + length/2])
            {
                // polygon(points=[[0,0],[0,3]]);
                square([0.1,length],center = true);
            }
        }
    }

    for(i=[0:60])
    {
        length = i%20 == 0 ? 6 : (i%10 == 0 ? 4.5 : (i%2 == 0 ? 3.5 : 2.5));
        rotate(45-i*1.5)
        {
            if(i%10 == 0)
            {
                translate([0,50])
                {
                    text(str(i/2),size=2.5,font="Abadi Extra Light",halign="center");
                }
            }
            translate([0,59 - length/2])
            {
                // polygon(points=[[0,0],[0,3]]);
                square([0.1,length],center = true);
            }
        }
    }
}