// upper_scale_distance = 64.3467;
upper_scale_distance = 61.75;
upper_scale = 100;
lower_scale_distance = 58.5;
lower_scale = 60;
scale_degree = 86;

mirror([1,0])
{
    color("black")
    {
        for(i=[upper_scale/-2:upper_scale/2])
        {
            length = i%10 == 0 ? 6 : (i%5 == 0 ? 4.5 : 3);
            rotate(0-i*scale_degree/upper_scale)
            {
                if(i%10 == 0)
                {
                    translate([0,upper_scale_distance + 7])
                    {
                        text(str(abs(i)),size=2.5,font="Abadi Extra Light",halign="center");
                    }
                }
                translate([0,upper_scale_distance + length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }
        rotate(scale_degree/2)
        {
            translate([0,upper_scale_distance+0.5])
            {
                text("- ",size=4,font="Abadi Extra Light",halign="right");
            }
        }
        rotate(scale_degree/-2)
        {
            translate([0,upper_scale_distance+0.5])
            {
                text(" +",size=4,font="Abadi Extra Light",halign="left");
            }
        } 
        for(i=[lower_scale/-2:lower_scale/2])
        {
            length = i%20 == 0 ? 6 : (i%10 == 0 ? 4.5 : (i%2 == 0 ? 3.5 : 2.5));
            rotate(0-i*scale_degree/lower_scale)
            {
                if(i%10 == 0)
                {
                    translate([0,lower_scale_distance-9])
                    {
                        text(str(abs(i/2)),size=2.5,font="Abadi Extra Light",halign="center");
                    }
                }
                translate([0,lower_scale_distance - length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }
        rotate(scale_degree/2)
        {
            translate([0,lower_scale_distance-3.5])
            {
                text("- ",size=4,font="Abadi Extra Light",halign="right", valign="bottom");
            }
        }
        rotate(scale_degree/-2)
        {
            translate([0,lower_scale_distance-3.5])
            {
                text(" +",size=4,font="Abadi Extra Light",halign="left", valign="bottom");
            }
        }     
        // Allign cross
        square([0.1,10],center = true);
        square([10,0.1],center = true);
    }
}