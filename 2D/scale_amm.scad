upper_scale_distance = 61.75;
lower_scale_distance = 58.5;
upper_scale2_distance = 45;
lower_scale2_distance = 45;


log_scale = [[0,1,0],[0.2,0,0],[0.4,0,0],[0.6,0,0],[0.8,0,0],
             [1,1,0],[1.2,0,0],[1.4,0,0],[1.6,0,0],[1.8,0,0],
             [2,1,0],[2.2,0,0],[2.4,0,0],[2.6,0,0],[2.8,0,0],
             [3,1,0],[3.2,0,0],[3.4,0,0],[3.6,0,0],[3.8,0,0],
             [4,1,0],[4.2,0,0],[4.4,0,0],[4.6,0,0],[4.8,0,0],
             [5,1,0],[5.2,0,0],[5.4,0,0],[5.6,0,0],[5.8,0,0],
             [6,1,0],[6.5,0,0],[7,1,0],[7.5,0,0],[8,1,0],
             [8.5,0,0],[9,1,0],[9.5,0,0],[10,1,0],[11,0,0],
             [12,0,0],[13,0,0],[14,0,0],[15,1,0],[16,0,0],
             [17,0,0],[18,0,0],[19,0,0],[20,1,0],[22,0,0],
             [24,0,0],[26,0,0],[28,0,0],[30,1,0],[35,0,0],
             [40,1,0],[45,0,0],[50,1,0],[60,0,0],[70,0,0],
             [80,0,0],[90,0,0],[100,1,3.5],[150,0,0],[200,1,3.5],[500,1,10],[1000,1,3.55]];

mirror([1,0])
{
    color("black")
    {
        for(i=[0:66])
        {
            length = log_scale[i][1] == 1 ? (log_scale[i][2] == 0 ? 3.5 : log_scale[i][2]) : 2;
            rotate(-45+90*(10/(10 + log_scale[i][0])))
            {
                if(log_scale[i][1] == 1)
                {
                    translate([0,upper_scale_distance + length + 1])
                    {
                        rotate(log_scale[i][2] == 0 ? 0 : 90)
                        {
                            text(str(log_scale[i][0]),
                                size=1.8,
                                font="Abadi Extra Light",
                                halign=(log_scale[i][2] == 0 ? "center" : "left"),
                                valign=(log_scale[i][2] == 0 ? "baseline" : "center"));
                        }
                    }
                }
                translate([0,upper_scale_distance + length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }
        rotate(-45)
        {
            translate([0,upper_scale_distance + 3.5/2])
            {
                square([0.1,3.5],center = true);
            }
            translate([0,upper_scale_distance + 3.5])
            {
                square([1,0.1]);
            }
            translate([1,upper_scale_distance + 3.5])
            {
                square([0.1,2]);
            }
            translate([1,upper_scale_distance + 6.5])
            {        
                rotate(180)
                {
                    scale([0.7,1])
                    {
                        text(str(8),
                            size=1.8,
                            font="Abadi Extra Light",
                            halign="center",
                            valign="top");
                    }
                }
            }
        }
        rotate(45)
        {
            translate([0,upper_scale_distance+0.5])
            {
                text("Ω ",size=2.5,font="Abadi Extra Light",halign="right");
            }
        }
        rotate(-45)
        {
            translate([0,upper_scale_distance+0.5])
            {
                text(" Ω",size=2.5,font="Abadi Extra Light",halign="left");
            }
        }    
        
        for(i=[0:50])
        {
            length = i%5 == 0 ? 3.5 : 2;
            rotate(45-i*1.8)
            {
                if(i%5 == 0)
                {
                    translate([0,lower_scale_distance - length - 1])
                    {
                        text(str(i/5),size=1.8,font="Abadi Extra Light",halign="center",valign="top");
                    }
                }
                translate([0,lower_scale_distance - length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }
        rotate(45)
        {
            translate([0,lower_scale_distance-1.5])
            {
                text("= ",size=2.5,font="Abadi Extra Light",halign="right", valign="top");
            }
        }
        rotate(-45)
        {
            translate([0,lower_scale_distance-1.5])
            {
                text(" =",size=2.5,font="Abadi Extra Light",halign="left", valign="top");
            }
        } 
        rotate(45)
        {
            translate([0,lower_scale_distance-0.5])
            {
                text("~ ",size=2.5,font="Abadi Extra Light",halign="right", valign="top");
            }
        }
        rotate(-45)
        {
            translate([0,lower_scale_distance-0.5])
            {
                text(" ~",size=2.5,font="Abadi Extra Light",halign="left", valign="top");
            }
        } 
        
        for(i=[0:179])
        {
            rotate(45-(i/2))
            translate([0,upper_scale2_distance])
            {
                square([upper_scale2_distance*PI/360,0.1]);
            }
        }    

        for(i=[0:30])
        {
            length = i%5 == 0 ? 3.5 : 2;
            rotate(45-i*3)
            {
                if(i%5 == 0)
                {
                    translate([0,upper_scale2_distance + length + 1])
                    {
                        text(str(i/10),size=1.8,font="Abadi Extra Light",halign="center");
                    }
                }
                translate([0,upper_scale2_distance + length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }

        rotate(45)
        {
            translate([0,upper_scale2_distance+0.5])
            {
                text("= ",size=2.5,font="Abadi Extra Light",halign="right");
            }
        }
        rotate(-45)
        {
            translate([0,upper_scale2_distance+0.5])
            {
                text(" =",size=2.5,font="Abadi Extra Light",halign="left");
            }
        } 
        rotate(45)
        {
            translate([0,upper_scale2_distance+1.8])
            {
                text("~ ",size=2.5,font="Abadi Extra Light",halign="right");
            }
        }
        rotate(-45)
        {
            translate([0,upper_scale2_distance+1.8])
            {
                text(" ~",size=2.5,font="Abadi Extra Light",halign="left");
            }
        } 

        for(i=[0:30])
        {
            length = i%5 == 0 ? 3.5 : 2;
            rotate(45-(i<2?i*0.2:i-1.15)*3)
            {
                if(i%5 == 0)
                {
                    translate([0,lower_scale2_distance - length - 1])
                    {
                        text(str(i/10),size=1.8,font="Abadi Extra Light",halign="center",valign="top");
                    }
                }
                translate([0,lower_scale2_distance - length/2])
                {
                    square([0.1,length],center = true);
                }
            }
        }
        rotate(45)
        {
            translate([0,lower_scale2_distance-1])
            {
                text("3V~ ",size=2,font="Abadi Extra Light",halign="right", valign="top");
            }
        }
        rotate(-45)
        {
            translate([0,lower_scale2_distance-1])
            {
                text(" 3V~",size=2,font="Abadi Extra Light",halign="left", valign="top");
            }
        } 

        // Allign cross
        square([0.1,10],center = true);
        square([10,0.1],center = true);
    }
}